window.jsHelpers = window.jsHelpers || {};

"use strict";


// Generic validation provider. 
// Core functionality is independant of the UI, 
// however UI hooking mechanis is also provided.
window.jsHelpers.validate = new function jsValidate() {
    
    
    // Validate a single value.
    //  value: the value to validate
    //  rules: oject defining rules and values to validate against.
    // Returns a 'validateValueResult'.
    this.validateValue = function(value, rules)
    {
        
    };
    
    
    // Validate a set of values against corresponding rules.
    //  valuesAndRules: An object
    // Returns a 'validateSetResult'.
    this.validateSet = function(valuesAndRules)
    {
        valuesAndRules = [
                defineValueRules('field-1-reference', 'field value', ' ruleSet object')
            ,
                defineValueRules('field-1-reference', 'field value', ' ruleSet object')
        ];
    };
    
    
    
    
    
    //-- Interna helper functions
    function defineValueRules(id, value, ruleSet)
    {
        return {
            id: id,
            value: value,
            rules: ruleSet
        };
    }
    
    
    
    
    
    
    
    
    
    
    
    //-- Global rules
    // Predefined rules that can be use in all validation instances.
    // A rule is a function that takes a value, plus optional parameters,
    // and returns true or false if the internal logic passes.
    this.globalRules = {
        
        // Return true if the value actually has a value,
        // or false if the value is undefined, null, or '' (zero length string).
        required: function(value) {
            var noValue = (value == undefined || value == null || value == "");
            
            return !noValue;
        },
        
        
        maxLength: function (value, maxAllowedLength)
        {
            
        },
        
        minLength: function (value, minAllowedLength)
        {
            
        },
        
        // Numberic only
        mustBeGreaterThan: function (value, minAllowedValue)
        {
            
        },
        
        // Numberic only
        mustBeLessThan: function (value, maxAllowedValue)
        {
            
        },
        
        
        // Any primative value. Uses loose "=="" coersion.
        equals: function (value, valueToEqual)
        {
            
        },
        
        // Array based (no object properties)
        containsValue: function(value, arrayOfValues)
        {
            
        }
        
    };
    
    
}