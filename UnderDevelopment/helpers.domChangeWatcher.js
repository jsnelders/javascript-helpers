/*global MutationObserver*/
/*global $*/
window.xxHelpers = window.jsHelpers || {};

"use strict";

// NOTE: This works as is for "simple" examples, however contains information, pointers, and
//       a start for more complex code if you require it.
 
// Dependencies:
//  jQuery
//
// Watch for changes in the DOM.
//  options.jqDynamicElementSelector: jQuery selector for the element(s) to observe.
//  options.changeType: type of change to monitor - "create" (element created), "remove" (element removed), "content" (content in element changes)
//  options.callbackFunction: function to call when mutation event happens.
window.jsHelpers.domChangeWatcher = function(options)
{
    var changeHandlerExecuted = false; // Set to true once DOM change handler has been executed.

    // "Old" way of observing DOM changes.
    //function observeChangesWithDOMSubtreeModified(jqDynamicElementSelector)
    //{
    //    var dynamicElement = $(jqDynamicElementSelector)[0];
    //    dynamicElement.addEventListener('DOMSubtreeModified', function () {
    //        domChangeHandler();
    //    }, false);
    //}

    // "Current" way of observicing DOM changes
    //  jqDynamicElementSelector: jQuery selector for the element(s) to observe.
    //  changeType: type of change to monitor - "create" (element created), "remove" (element removed), "content" (content in element changes)
    //  callbackFunction: function to call when mutation event happens.
    function observeChangesWithMutationObserver(jqDynamicElementSelector, changeType, callbackFunction)
    {
        var mutationObserver = new MutationObserver(function(mutationRecords) 
        {
            if (mutationRecords.length == 1)
            {
                if (changeType === 'create') 
                {
                    domChangeHandler(callbackFunction);
                }
                else if (changeType === 'remove') 
                {
                    domChangeHandler(callbackFunction);
                }
                else if (changeType === 'content' && mutationRecords[0].type === 'characterData') 
                {
                    domChangeHandler(callbackFunction);
                }
                else
                {
                    throw "observeChangesWithMutationObserver(): Unknown changeType '" + changeType + "'.";
                }
            }
        });

        var whatToObserve = {childList: true, subtree: true, characterData: true };
        var dynamicElement = $(jqDynamicElementSelector)[0];
        mutationObserver.observe(dynamicElement, whatToObserve);
    }

    function domChangeHandler(callbackFunction)
    {
        //TODO: Now we do something because the DOM has cahnged.
        callbackFunction();
        
        changeHandlerExecuted = true;
    }

    // Watch for change.    
    var jqDynamicElementSelector = options.jqDynamicElementSelector;
    var changeType = options.changeType;
    var callbackFunction = options.callbackFunction;
    observeChangesWithMutationObserver(jqDynamicElementSelector, changeType, callbackFunction);


    // // If no change after 500ms, update content for selected product.
    // setTimeout(function() {
    //     if (changeHandlerExecutred == false)
    //     {
    //         // DOM change not detected. May have already fired before observer set. Trigger new content now.
    //         // Maybe do something else here
    //  
    //         // TODO: Handle a "timeout" scenario here.
    //
    //         changeHandlerExecutred = true;
    //     }
    // }, 500);
};
