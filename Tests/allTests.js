/*global expect*/

// Tests written with Jasmine 2.4.1
// See: http://jasmine.github.io/2.4/introduction.html



// Define the "helpers" alias for the base helper object.
var helpers = window.jsHelpers;








describe("helpers.cookies", function() 
{

    it("window.jsHelpers.cookies is alive", function() {
        expect(window.jsHelpers.cookies).toBeTruthy();
    });
    
    
    it("create, read and clear a simple cookie", function() {
        // Set a simple cookie, with 20 minute expiry.
        helpers.cookies.setCookie("simpleTestCookie", "myValue", 20);
        
        // Read the cookie.
        var simpleCookieValue = helpers.cookies.getCookie("simpleTestCookie");
        expect(simpleCookieValue).toBeTruthy();
        
        // Clear the cookie
        helpers.cookies.clearCookie("simpleTestCookie");
        
        // Ensure cookie cleared
        simpleCookieValue = helpers.cookies.getCookie("simpleTestCookie");
        expect(simpleCookieValue).toBe(null);
    });
    
    
    
    it("create, read and clear a complex (JSON string) cookie", function() {
        // Set a complex cookie, with 30 minute expiry.
        var complexTestObject = {
            ID: 107292,
            name: "Test object",
            description: "This is a test.\Beep!",
            timestamp: new Date().toJSON(),
            timestamp2: "2016-05-09T04:28:16.996Z"
        };
        var complexTestObjectJson = JSON.stringify(complexTestObject);
        helpers.cookies.setCookie("complexTestCookie", complexTestObjectJson, 30);
        
        // Read complex cookie
        var complexTestCookieValue = helpers.cookies.getCookie("complexTestCookie");
        expect(complexTestCookieValue).toBeTruthy();
        
        // Clear the cookie
        helpers.cookies.clearCookie("complexTestCookie");
        
        // Ensure cookie cleared
        complexTestCookieValue = helpers.cookies.getCookie("complexTestCookie");
        expect(complexTestCookieValue).toBe(null);
    });
    

});









describe("helpers.debug", function() 
{
    it("window.jsHelpers.cookies is alive", function() {
        expect(window.jsHelpers.debug).toBeTruthy();
    });
    
    //TODO: Not tests written yet.
});






describe("helpers.getUrlParts", function() 
{
    it("window.jsHelpers.getUrlParts is alive", function() {
        expect(window.jsHelpers.getUrlParts).toBeTruthy();
    });
    
    
    it("full URL test 1", function() {
        expect(true).toBe(true);
        
        var url = encodeURI("https://test.mydomain.com.au/some/big/path/to/a/resource?test=aValue&switch&multi=1&multi=2&multi=something, more&anotherMulti=&anotherMulti=X#something");
        var urlParts = helpers.getUrlParts(url);
        
        //var json = JSON.stringify(urlParts, null, 3);
        
        expect(urlParts.getSubDomain("mydomain.com.au")).toBe("test");
        expect(urlParts.getSubDomain("mydomain2.com.au")).toBe("");
        
        expect(urlParts.getQueryByKey("test")[0]).toBe("aValue");
        
        expect(urlParts.getQueryByKey("switch").length).toBe(1);
        expect(urlParts.getQueryByKey("switch")[0]).toBe("");
        
        expect(urlParts.getQueryByKey("foo")).toBe(null);
        
        expect(urlParts.getQueryByKey("multi")[0]).toBe("1");
        expect(urlParts.getQueryByKey("multi")[1]).toBe("2");
        expect(urlParts.getQueryByKey("multi")[2]).toBe("something, more");
        
        expect(urlParts.getQueryByKey("anothermulti")[0]).toBe("");
        expect(urlParts.getQueryByKey("anothermulti")[1]).toBe("X");
    });

    
    

    
    
    it("full URL test 2", function() {
        expect(true).toBe(true);
        
        var url = encodeURI("https://test.mydomain.com.au/pathToResource");
        var urlParts = helpers.getUrlParts(url);
        
        var json = JSON.stringify(urlParts, null, 3);
        expect(json).toBeTruthy();
    });
    
    
    it("full URL test 3", function() {
        var url = encodeURI("http://mydomain.com.au");
        var urlParts = helpers.getUrlParts(url);
    
        var json = JSON.stringify(urlParts, null, 3);
        expect(json).toBeTruthy();
    });
});









describe("helpers.isPage", function() 
{
    it("window.jsHelpers.isPage is alive", function() {
        expect(window.jsHelpers.isPage).toBeTruthy();
    });
    
    it("Full URL with 1 level, path mismatch", function() {
        expect(helpers.isPage("homepage", "https://www.test.com/some-page")).toBe(false);
    });
    
    it("Full URL with 1 level, path does match", function() {
        expect(helpers.isPage("homepage", "https://www.test.com/homepage")).toBe(true);
    });
    
    it("Raw root path '/' is equal to '/homepage'", function() {
        expect(helpers.isPage("homepage", "https://www.test.com/")).toBe(true);
    });
    
    it("Raw path of 1 level does not match", function() {
        expect(helpers.isPage("/something", "/somethingElse")).toBe(false);
    });
    
    it("Raw path of 2 levels do match", function() {
        expect(helpers.isPage("/two/levels", "/two/levels")).toBe(true);
    });
    
    it("Raw path of 2 levels matches full domain with query string and hash parts", function() {
        expect(helpers.isPage("/two/levels", "https://www.test.com/two/levels?a=x#b=2")).toBe(true);
    });
    
    it("Raw path of 2 levels does not match full domain with query string and hash parts", function() {
        expect(helpers.isPage("/two/levels2", "https://www.test.com/two/levels?a=x#b=2")).toBe(false);
    });
    
    it("Path match is not case sensitive.", function() {
        expect(helpers.isPage("/MIxED/CaSe", "https://www.test.com/mIXed/CasE?a=x#b=2")).toBe(true);
    });
    
    it("Expecting an error", function() {
        expect(helpers.isPage).toThrow("jsHelpers.pageIs(): pageName not specified");
    });

});










describe("helpers.polyfills", function() 
{
    it("window.jsHelpers.polyfills is alive", function() {
        expect(window.jsHelpers.polyfills).toBeTruthy();
    });

    // No tests yet.
});











describe("helpers.refererMatch", function() 
{
    it("window.jsHelpers.refererMatch is alive", function() {
        expect(window.jsHelpers.refererMatch).toBeTruthy();
    });
    
    it("Supplied referrerUrl successfully matches supplied domain and list of paths. ", function() {
        var options = {
            paths: ['/', '/widgets', 'ice-cream', 'unicorn'],
            domain: "www.test.com",
            refererUrl: "http://www.test.com/unicorn"
        };
        var match = helpers.refererMatch(options);
        
        expect(match).toBe(true);
    });
    
    
    it("Supplied referrerUrl but not match datam, should perform domain only match and return success", function() {
        var options = {
            //paths: ['/', '/widgets', 'ice-cream', 'unicorn'],
            //domain: "www.test.com",
            refererUrl: "http://" + document.location.hostname + "/unicorn"
        };
        var match = helpers.refererMatch(options);
        
        expect(match).toBe(true);
    });

});


















// useSessionStorage: true to ise sessionStorage, false to use cookies fallback
describe("helpers.session", function() 
{
    it("window.jsHelpers.session is alive", function() {
        expect(window.jsHelpers.session).toBeTruthy();
    });
    
    
    it("Set, get, and clear a simple value in sessionStorage", function() 
    {
        // Set the type of storage to use
        helpers.session.setStorageType("session");
    
        // Set a simple value.
        helpers.session.setValue("simpleSessionTest", "myValue");
        
        // Get the value back and make sure it's the same
        var retrievedValue = helpers.session.getValue("simpleSessionTest");
        expect(retrievedValue).toBe("myValue");
            
        // Clear the value from session
        helpers.session.clearValue("simpleSessionTest");
        
        // Ensure value is cleared.
        retrievedValue = helpers.session.getValue("simpleSessionTest");
        expect(retrievedValue).toBe(null);
    });
    
    it("Set, get, and clear a complex value in sessionStorage", function() 
    {
        // Set the type of storage to use
        helpers.session.setStorageType("session");
        
        // Set a complex value in session.
        var testObject = {
            ID: 107292,
            name: "Test object",
            description: "This is a test.\Beep!",
            timestamp: new Date().toJSON(),
            timestamp2: "2016-05-09T04:28:16.996Z"
        };
        var testValue = JSON.stringify(testObject);
        helpers.session.setValue("complexSessionTest", testValue);
        
        
        // Get the value back and make sure it's the same
        var retrievedValue = helpers.session.getValue("complexSessionTest");
        expect(retrievedValue).toBe(testValue);
            
        // Clear the value from session
        helpers.session.clearValue("complexSessionTest");
        
        // Ensure value is cleared.
        retrievedValue = helpers.session.getValue("complexSessionTest");
        expect(retrievedValue).toBe(null);
    });
    
    
    it("Set, get, and clear a simple value in cookie storage fallback", function() 
    {
        // Set the type of storage to use
        helpers.session.setStorageType("cookie");
    
        // Set a simple value.
        helpers.session.setValue("simpleSessionTest", "myValue");
        
        // Get the value back and make sure it's the same
        var retrievedValue = helpers.session.getValue("simpleSessionTest");
        expect(retrievedValue).toBe("myValue");
            
        // Clear the value from session
        helpers.session.clearValue("simpleSessionTest");
        
        // Ensure value is cleared.
        retrievedValue = helpers.session.getValue("simpleSessionTest");
        expect(retrievedValue).toBe(null);
    });
    
    it("Set, get, and clear a complex value in cookie storage fallback", function() 
    {
        // Set the type of storage to use
        helpers.session.setStorageType("cookie");
        
        // Set a complex value in session.
        var testObject = {
            ID: 107292,
            name: "Test object",
            description: "This is a test.\Beep!",
            timestamp: new Date().toJSON(),
            timestamp2: "2016-05-09T04:28:16.996Z"
        };
        var testValue = JSON.stringify(testObject);
        helpers.session.setValue("complexSessionTest", testValue);
        
        
        // Get the value back and make sure it's the same
        var retrievedValue = helpers.session.getValue("complexSessionTest");
        expect(retrievedValue).toBe(testValue);
            
        // Clear the value from session
        helpers.session.clearValue("complexSessionTest");
        
        // Ensure value is cleared.
        retrievedValue = helpers.session.getValue("complexSessionTest");
        expect(retrievedValue).toBe(null);
    });

});








describe("helpers.tryExecute", function() 
{
    it("window.jsHelpers.tryExecute is alive", function() {
        expect(window.jsHelpers.tryExecute).toBeTruthy();
    });
    
    
    
        
    it("execute with success response", function() 
    {
        var f1 = function testFunction1()
        {
            return true;
        };
        
        helpers.tryExecute(f1, function tryExecuteCallback(result)
        {
            expect(result).toBe(1); // success
        });
        
    });


    
    
    
        
    it("executed with simulated 'wait' and success response", function() 
    {
        window.testCounter = 0;
        function testFunction()
        {
            window.testCounter++;
            if (window.testCounter <= 3)
            {
                return false;
            }
            
            return true;
        }
        helpers.tryExecute(testFunction, function tryExecuteCallback(result)
        {
            expect(result).toBe(1); // success
            
            window.testCounter = null;
        });
        
    });


    
    
    
        
    it("executed with with simulated timeout", function() 
    {
        window.testCounter = 0;
        function testFunction()
        {
            window.testCounter++;
            if (window.testCounter <= 50)
            {
                return false;
            }
            return true;
        }
        helpers.tryExecute(testFunction, function tryExecuteCallback(result)
        {
            expect(result).toBe(-3);  // timeout
            window.testCounter = null;
        });
    
        
    });


    
    
        
    it("executed with bad response (function returns unexpected value)", function() 
    {
        function testFunction()
        {
            console.log("testFunction(): Completed with bad response.");
        }
        helpers.tryExecute(testFunction, function tryExecuteCallback(result)
        {
            expect(result).toBe(-2);    // bad response
        });
    });


    
    
    
        
    it("executed function threw an error", function() 
    {
        // try
        // {
        //     helpers.tryExecute();   // Expecting console.error about missing function;
        // }
        // catch (e)
        // {
        //     var error = JSON.stringify(e);
        // }
        expect(helpers.tryExecute).toThrow();
        
    });
    
    
        
    it("executed successfully but did not call a callback function with a response", function() 
    {
        var executedFunctionDidSomething = "";
        function testFunction()
        {
            executedFunctionDidSomething = "yes";
            return true;
        }
        helpers.tryExecute(testFunction);  // Expecting normal execution, but no callback to respond.Array
        
        setTimeout(function() {
            // Set a timeout to give tryExecute time to finish, so we can test the value of executedFunctionDidSomething.
            // Not pretty, but it works for a test to verify testFunction() actually did execute correctly.
            expect(executedFunctionDidSomething).toBe("yes");
        }, 20);
    });
    

});