// Tests for setting and getting sessionStorage
// Info about sessionStorage at http://caniuse.com/#search=sessionStorage


// Test helper. Write to required output location.
function out(value, type)
{
    console.log("SesstionStorageTest(): " + value);
    
    type = type || "log"; type = type.toLowerCase();
    if (!(type == "log" || type == "warn" || type == "error"))
    {
        type = "log";
    }
    
    var typeClass = "type-" + type;
    
    var eleOut = document.getElementById("out");
    var html = eleOut.innerHTML;
    
    if (html)
    {
        html = html + "\n";
    }
    
    html = html + "<pre class='" + typeClass + "'>" + value + "</pre>";
    
    eleOut.innerHTML = html;
}


function accessingDOMSessionStorageExample()
{
    // Define a value to set and get
    var testObject = {
        ID: 107292,
        name: "Test object",
        description: "This is a test.\Beep!",
        timestamp: new Date().toJSON(),
        timestamp2: "2016-05-09T04:28:16.996Z"
    };
    
    var testString = JSON.stringify(testObject);
    
    out("testString=" + testString);
    
    testObject = null;
    
    
    // Set a value in storage
    window.sessionStorage.setItem("myTestObject", testString);
    
    // Get the value from storage
    var retrievedObjectString = window.sessionStorage.getItem("myTestObject");
    var retrievedObject = JSON.parse(retrievedObjectString);
    
    out("retrievedObjectString=" + retrievedObjectString);
}

accessingDOMSessionStorageExample();