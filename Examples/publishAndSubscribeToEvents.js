/*global $*/

/// 
/// Helper class for managing events.
///
function EventManager() {
    var subscribers = [];
    
    // Return all subscriber function for an event.
    function getEventSubscribers(eventType)
    {
        var eventSubscribers = [];
        
        for (var i = 0; i < subscribers.length; i++)
        {
            if (subscribers[i].eventType == eventType)
            {
                eventSubscribers.push(subscribers[i].subscriberFunction);
            }
        }
        
        return eventSubscribers;
    }
    
    
    // Register a new subscriber function for an event type.
    this.registerEventSubscriber = function(eventType, subscriberFunction)
    {
        subscribers.push({ eventType: eventType, subscriberFunction: subscriberFunction });
    };

    // Notify all subscriber functions for an event type that the even is raised, 
    // and pass along data from the event (via the inspected arguments[] array).
    this.notify = function(eventType)
    {
        var subscribersToNotify = getEventSubscribers(eventType);
        
        // Get event data from the 2nd-to-nth elements of the arguments array.
        var eventData = [];
        for (var i = 1; i < arguments.length; i++)
        {
            eventData.push(arguments[i]);
        }
        
        // Notify subscribers, and pass event data along.
        for (var i = 0; i < subscribersToNotify.length; i++)
        {
            subscribersToNotify[i].apply(this, eventData);
        }
    };
}


///
/// Example business logic class with event publishing.
///
///     eventManager: Instance of the EventManager helper class.
///
function BusinessLogic(eventManager)
{
    // Define events ("log" and "error").
    function raiseLog(code, message)
    {
        eventManager.notify("log", code, message);
    }
    this.onLog = function(subscriberFunction)
    {
        eventManager.registerEventSubscriber("log", subscriberFunction);
    };
    
    function raiseError(type, message)
    {
        eventManager.notify("error", type, message);
    }
    this.onError = function(subscriberFunction)
    {
        eventManager.registerEventSubscriber("error", subscriberFunction);
    };
    
    
    //-- Business logic that raises events
    this.add = function(arg1, arg2)
    {
        var equals = arg1 + arg2;
        
        if (equals >= 0)
        {
            raiseLog("success", arg1 + " + " + arg2 + " = " + equals);
        }
        else
        {
            raiseError("negative", arg1 + " + " + arg2 + " = " + equals);
        }
    };
}



///
/// Example event subscriber.
///
function ConsoleLogger()
{
    this.notifyLog = function(code, message)
    {
        console.log(code, message);
    };
    this.notifyError = function(type, message)
    {
        console.error(type, message);
    };
}


///
/// Example application.
///
function main()
{
    var consoleLogger = new ConsoleLogger();
    var bl = new BusinessLogic(new EventManager());
    
    // Subscribe to events.
    bl.onLog(consoleLogger.notifyLog);
    bl.onError(consoleLogger.notifyError);
    
    // Do some logic that raises events
    bl.add(1, 2);
    bl.add(6, 7);
    bl.add(5, -12);
}
main();