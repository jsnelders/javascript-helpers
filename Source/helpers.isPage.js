window.jsHelpers = window.jsHelpers || {};

"use strict";
 
window.jsHelpers.isPage = function(pagePath, pagePathOrUrl)
{
    var isPage = false;
    
    if (!pagePath)
    {
        throw "jsHelpers.pageIs(): pageName not specified";
        //return false;
    }
    
    pagePathOrUrl = pagePathOrUrl || document.location.pathname;
    
    // Parse the real pathname from, regardless of wither a 
    // partial path (e.g. /some/path?a=x) was supplied, or wither a full
    // URL (e.g. http://www.example.com/some/path?name=value#anchor) was supplied.
    var url = document.createElement('a');
    url.href = pagePathOrUrl;
    var cPagePath = url.pathname;
    
    // Make lower case for easier comparison.
    cPagePath = cPagePath.toLowerCase();
    
    pagePath = pagePath.toLowerCase();  // Add leading "/" for pagePath comparison.
    var cPageName = pagePath;
    if (cPageName.indexOf("/") == -1) cPageName = "/" + cPageName;    // Version Used for comparison. Leading "/" for pagePath comparison.
    
    if (cPageName === "/homepage" || cPageName === "/")
    {
        // Home page path may be "/" or "/homepage"
        if (cPagePath === "/" ||                         // e.g. "/"
            cPagePath === "/homepage" ||                 // e.g. "/homepage"
            cPagePath === "/homepage.htm" ||             // e.g. "/homepage.htm..." OR "/homepage.html..."
            cPagePath === "/homepage/"                   // e.g. "/homepage/...."
           )
           {
               isPage = true;
           }
    }
    else
    {
        // All other pages
        if (cPagePath === cPageName ||                   // e.g. "/adsl"
            cPagePath === cPageName + ".htm" ||          // e.g. "/adsl.htm..." OR "/adsl.html..."
            cPagePath === cPageName + "/"                // e.g. "/adsl/...."
           )
        {
            isPage = true;
        }
    }
    
    return isPage;
};
