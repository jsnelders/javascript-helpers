window.jsHelpers = window.jsHelpers || {};

"use strict";

// requires: jQuery
window.jsHelpers.trackElementScrolling = function(options)
{
    // Initialise
    var bottomReached = false;
    var maxScrollBottomPositionReached = 0;
    
    var outerElement = $(options.scrollableElementSelector);
    
    if (outerElement.length == 0)
    {
        // Element not found.
        if (options.notFound) options.notFound();
        return;
    }
    
    var scrollHeight = outerElement[0].scrollHeight;    // Including padding and borders.
    var initialScrollHeight;
    initialScrollHeight = scrollHeight;
    var containerHeight = outerElement.innerHeight();   // Needs to include any content padding and borders.
    
    if (options.initialised) options.initialised({
        containerHeight: containerHeight,   //innerHeight, 
        scrollHeight: scrollHeight 
    });
    
    outerElement.scroll(function() {
        var element = $(this);
        var scrollTop = element.scrollTop();
        var scrollBottom = scrollTop + containerHeight;
        
        if (scrollBottom > maxScrollBottomPositionReached) maxScrollBottomPositionReached = scrollBottom;
        
        // Update scroll height in case height changed since initialisation (eg. DOM sorting itself out)
		scrollHeight = outerElement[0].scrollHeight;
        
        if(scrollTop + containerHeight >= scrollHeight) 
        {
            if (options.bottomReached) options.bottomReached({
                containerHeight: containerHeight,
                initialScrollHeight: initialScrollHeight,
                scrollHeight: scrollHeight,     // Current scroll height
                maxScrollBottomPositionReached: maxScrollBottomPositionReached
            });
            bottomReached = true;
        } 
        else 
        {
            if (options.scroll) options.scroll({
                containerHeight: containerHeight,
                initialScrollHeight: initialScrollHeight,
                scrollHeight: scrollHeight,     // Current scroll height
                scrollTop: scrollTop,
                scrollBottom: scrollBottom,
                maxScrollBottomPositionReached: maxScrollBottomPositionReached,
                bottomReached: bottomReached
            });
        }
    });
};