window.jsHelpers = window.jsHelpers || {};

"use strict";

window.jsHelpers.session = new function Session() 
{
    var cookies = window.jsHelpers.cookies;
    
    var session = {
        
        setStorageType: function(type)
        {
            if (type == "cookie")
            {
                cookies.setCookie("sessionStorageType", "cookie");
            }
            else if (type == "session")
            {
                cookies.setCookie("sessionStorageType", "session");
            }
            else
            {
                throw "Unknown type '" + type + "'. Expecting 'cookie' or 'session'.";
            }
        },
        
        sessionStorageAvailable: function()
        {
            // Via: https://gist.github.com/paulirish/5558557
            function storageAvailable(type) 
            {
                try 
                {
                    var storage = window[type],
                    x = '__storage_test__';
                    storage.setItem(x, x);
                    var y = storage.getItem(x);
                    storage.removeItem(x);
                    if (x != y) return false;
                    return true;
                }
                catch(e) 
                {
                    return false;
                }
            }
            return storageAvailable('sessionStorage');
        },
        
        sessionCookiesAvailable: function()
        {
            try
            {
                document.cookie = "testcookie=testcookie;expires=Tue, 01-Jan-2030 00:00:00 GMT";
                var cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
                document.cookie = "testcookie=;expires=-1"; // Clear the test cookie
                
                return (cookieEnabled); 
            }
            catch (e)
            {
                // Safe fallback
                return false;
            }
        },
        
        setValue: function(key, value)
        {
            if (this.useSessionStorage())
            {

                sessionStorage.setItem(key, value, value);
            }
            else
            {
                // Fall back to cookies.
                cookies.setCookie(key, value, value, 0);    // Set session expiry to when the browser closes
            }
        },
        
        getValue: function(key) 
        {
            if (this.useSessionStorage())
            {
                return sessionStorage.getItem(key);
            }
            else
            {
                // Fall back to cookies.
                return cookies.getCookie(key);
            }
        },
        
        clearValue: function(key)
        {
            if (this.useSessionStorage())
            {
                return sessionStorage.removeItem(key);
            }
            else
            {
                // Fall back to cookies.
                cookies.clearCookie(key);
            }
        },
        
        storageType: "",
        
        useSessionStorage: function()
        {
            var sessionStorageType = cookies.getCookie("sessionStorageType");
            if (sessionStorageType == "session")
            {
                return true;
            }
            else if (sessionStorageType == "cookie")
            {
                return false;
            }
            else
            {
                return true;   // Because, well, sessionStorage is what we're after.
            }
        },
    };
    return session;
};

// Setup session.
if (window.jsHelpers.session.sessionStorageAvailable())
{
    window.jsHelpers.session.setStorageType("session");
}
else
{
    if (window.jsHelpers.session.sessionCookiesAvailable() == false)
    {
        throw "jsHelpers.session(): sessionStorage AND cookies not available. Session Storage will not work";
    }
    window.jsHelpers.session.setStorageType("cookie");
}