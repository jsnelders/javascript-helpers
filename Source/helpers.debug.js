window.jsHelpers = window.jsHelpers || {};

"use strict";

window.jsHelpers.debug = {
    
    // Store all debug data records.
    records: [],
    
    // ID of the DOM element acting is a container for debug output. 
    // Leave blank for no output.
    documentOutputContainerID: "",
    
    // When writing to the console, prefix all output with this value.
    consolePrefix: "",
    

    /// [Internal] Handler for all debug processing.
    /// (Note: although publically available, it is logically an "internal" method, designed for use by other class methods.)
    /// 
    /// data: An object containing:
    /// data.arguments: The arguments array from one of the helper functions (log, warn, error).That is, the array of all parameters passed into the helper function.
    /// data.type: The type of debug output: "log", "warn", or "error".
    process: function(data) {
        if (!data.arguments || data.arguments.length == 0)
        {
            // No arguments.
            return;
        }
        
        // Give the data a timestamp, and store it.
        data.timestamp = new Date().toJSON();
        
        this.records.push[data];
        
        for (var a = 0; a < data.arguments.length; a++)
        {
            var argument = data.arguments[a];
            
            //-- Calculate how to display te argument
            var argumentDisplayValue = "";
            if (typeof argumentDisplayValue == "object") 
            {
                argumentDisplayValue = JSON.stringify(argument, null, 3);
            }
            else
            {
                argumentDisplayValue = argument.toString();
            }
            
            //-- Write to console
            if (data.type == "log") console.log(this.consolePrefix + " " + argumentDisplayValue);
            if (data.type == "warn") console.warn(this.consolePrefix + " " + argumentDisplayValue);
            if (data.type == "error") console.error(this.consolePrefix + " " + argumentDisplayValue);
            
            //-- Write to DOM outut
            if (this.documentOutputContainerID.length != "")
            {
                var eleOut = document.getElementById(this.documentOutputContainerID);
                var html = eleOut.innerHTML;
                
                if (html)
                {
                    html = html + "\n";
                }
                
                html = html + "<pre class='" + data.type + "'>" + argumentDisplayValue + "</pre>";
                
                eleOut.innerHTML = html;
            }
        }
    },
    
    log: function() {
        this.process({
            arguments: arguments,
            type: "log"
        });
    },
    
    warn: function() {
        this.process({
            arguments: arguments,
            type: "warn"
        });
    },
    
    error: function() {
        this.process({
            arguments: arguments,
            type: "error"
        });
    },
};