window.jsHelpers = window.jsHelpers || {};

"use strict";

// requires: jQuery
window.jsHelpers.domConsole = new function DomConsole()
{
    var triggerID = "dcTrigger";
    var consoleID = "dcConsole";
    var consoleState = "closed";
    var consoleHeight1 = "";
    var consoleHeight2 = "";
    
    
    // Initialise the DOM console.
    //  display: set to 'true' to show trigger immediately.
    //  height1: standard height of the console when displayed. Default: 200px.
    //  height2: extended height of the console. Default: 400px.
    this.init = function(display, height1, height2)
    {
        consoleHeight1 = height1 || "200px";
        consoleHeight2 = height2 || "400px";
        
        var triggerHtml = "";
        triggerHtml += "<div id='" + triggerID + "' style='display: none; position: absolute; z-index: 1000; top: 0; right: 0; width: 50px; height: 30px; line-height: 30px; text-align: center; font-size: 10px; opacity: .5; cursor: pointer; border: 1px solid #c0c0c0; background-color: #eeeeee; padding: 4px; font-family: sans-serif;' onClick='window.jsHelpers.domConsole.toggleConsole();'>";
        triggerHtml += "Console";
        triggerHtml += "</div>";
        
        $("body").prepend(triggerHtml);
        
        if (display)
        {
            this.showTrigger();
        }
        
        var consoleHtml = "";
        consoleHtml += "<div id='" + consoleID + "-outer' style='display: none; position: absolute; z-index: 1000; bottom: 0; left: 0; right: 0; width: 100%; height: " + consoleHeight1 + "; overflow: auto; background-color: #ffffff; border-top: 1px solid black;'>";
        consoleHtml += "  <div id='" + consoleID + "' style='position: relative; width: auto; height: auto; margin: 4px;'>";
        consoleHtml += "  </div>";
        consoleHtml += "</div>";
        
        $("body").append(consoleHtml);
    };
    
    
    // Show the console trigger.
    this.showTrigger = function()
    {
        $("#" + triggerID).css("display", "block");
    };
    
    // Hide just the console trigger (console will remain open if currently open).
    this.hideTrigger = function()
    {
        $("#" + triggerID).css("display", "none");
    };
    
    // Hide all console elements.
    this.hide = function()
    {
        this.hideTrigger();
        $("#" + consoleID + "-outer").css("display", "none");
        consoleState = "closed";
    };
    
    
    // Toggle the console between 3 states: 1) Open to standard height; 2) Still open, increase height; 3) Close console.
    this.toggleConsole = function()
    {
        var outer = $("#" + consoleID + "-outer");
        if (consoleState == "closed")
        {
            // Open the console.
            outer.css("display", "block");
            outer.css("height", consoleHeight1);
            consoleState = "open1";
        }
        else if (consoleState == "open1")
        {
            // Increase the height on 1st toggle while open
            outer.css("height", consoleHeight2);
            consoleState = "open2";
        }
        else
        {
            // Close the console on 2nd toggle while open.
            outer.css("display", "none");
            outer.css("height", consoleHeight1);
            consoleState = "closed";
        }
    };
    
    
    // Add up to 3 times to the console. Write to both the browser developer console and the DOM console.
    // (3 items in the entry should be more than enough for most cases.)
    this.log = function(arg1, arg2, arg3)
    {
        function argToString(arg)
        {
            if (typeof arg == "undefined") return "undefined";
            if (arg == null) return "null";
            if (typeof arg == "function") return JSON.stringify(arg);
            if (typeof arg == "object") return JSON.stringify(arg);
            if (typeof arg == "boolean") return arg.toString();
            if (typeof arg == "number") return arg.toString();
            if (typeof arg == "string") return arg.toString();
            if (typeof arg == "symbol") return arg.toString();

            return arg;
        }
        
        var logDate = new Date().toJSON();
        
        var dcConsole = $("#" + consoleID);
        if (arg1)
        {
            console.log(arg1);
            dcConsole.append("<pre class='log' data-data='" + logDate + "' data-item='1'>" + argToString(arg1) + "<pre>");
        }
        if (arg2)
        {
            console.log(arg2);
            dcConsole.append("<pre class='log' data-data='" + logDate + "' data-item='2'>" + argToString(arg2) + "<pre>");
        }
        if (arg3)
        {
            console.log(arg3);
            dcConsole.append("<pre class='log' data-data='" + logDate + "' data-item='3'>" + argToString(arg3) + "<pre>");
        }
    };
};