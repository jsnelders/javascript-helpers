window.jsHelpers = window.jsHelpers || {};

"use strict";

window.jsHelpers.cookies = new function Cookies() 
{
    return {
        
        setCookie: function(name, value, expiryMinutes)
        {
            try 
            {
                if (!expiryMinutes)
                {
                    expiryMinutes = 0;
                }
                
                var expires;
                if (expiryMinutes != 0)
                {
                    var date = new Date();
                    date.setTime(date.getTime() + (expiryMinutes*60*1000));
                    expires = "expires="+ date.toUTCString();   //.toGMTString();
                }
                else
                {
                    expires = "";
                }
                
                document.cookie = name + "=" + value + "; " + expires;  // No "path=" set, so cookie only for current domain.
                
                return true;
            } 
            catch (e) 
            {
                return false;
            }
        },
        
        getCookie: function(name) 
        {
            try 
            {
                name = name + "=";
                
                var cookies = document.cookie.split(';');
                
                for(var i = 0; i <cookies.length; i++) 
                {
                    var cookie = cookies[i];
                    while (cookie.charAt(0)==' ') 
                    {
                        //cookie = cookie.substring(1);
                        cookie = cookie.substring(1, cookie.length);
                    }
                    if (cookie.indexOf(name) == 0) 
                    {
                        return cookie.substring(name.length, cookie.length);
                    }
                }
                
                return null;
            }
            catch (e)
            {
                return null;
            }
        },
        
        clearCookie: function(name)
        {
            return this.setCookie(name, "", -1);
        }
    };
};