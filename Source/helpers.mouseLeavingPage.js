window.jsHelpers = window.jsHelpers || {};

"use strict";

window.jsHelpers.mouseLeavingPage = function(options)
{
    // Tracker to ensure exit callback is only called once.
    var onceExecutedCallback = false;
    
    // Core function, calculating mouse position and controlling callback.
    function trackMouseMove(event) 
    {
        var dot, eventDoc, doc, body, pageX, pageY;

        event = event || window.event; // IE-ism

        // Source: http://stackoverflow.com/questions/7790725/javascript-track-mouse-position
        // If pageX/Y aren't available and clientX/Y are,
        // calculate pageX/Y - logic taken from jQuery.
        // (This is to support old IE)
        if (event.pageX == null && event.clientX != null) {
            eventDoc = (event.target && event.target.ownerDocument) || document;
            doc = eventDoc.documentElement;
            body = eventDoc.body;

            event.pageX = event.clientX +
              (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
              (doc && doc.clientLeft || body && body.clientLeft || 0);
            event.pageY = event.clientY +
              (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
              (doc && doc.clientTop  || body && body.clientTop  || 0 );
        }
        
        if (options.topExitY && (event.pageY < options.topExitY))
        {
            // Exiting the top of the page.
            //console.warn("Existing: " + event.pageY + " (mouse) < (top) " + options.topExitY);
            
            if (options.topExitCallback && onceExecutedCallback == false)
            {
                // Ensure callback only executed once.
                onceExecutedCallback = true;
                
                // Execite the callback
                options.topExitCallback(event.pageY, options.topExitY);
            }
        }
    }
    
    
    // Setup the tracking, and update the 'options'.
    function execute()
    {
        var body = document.body;
        var html = document.documentElement;
        var documentHeight = Math.max(body.scrollHeight, body.offsetHeight, 
                                html.clientHeight, html.scrollHeight, html.offsetHeight);
                                
        if (options.topExitYpx)
        {
            // Explicity setting from the sepcifiex pixel (px) position.
            options.topExitY = options.topExitYpx;
        }
        else if (options.topExitYpc)
        {
            // calculating from the specified percentage (%) from top position.
            options.topExitY = documentHeight * (options.topExitYpc / 100);
        }
        else
        {
            // Default if nothing set. It shouldn't track an exit.
            options.topExitY = 0;
        }
        
        document.onmousemove = function (event) {
            trackMouseMove(event, options)
        };
    }
    
    // Wait for everything to be loaded.
    window.onload = function()
    {
        execute();
    }
}


 
window.jsHelpers.isPage = function(pagePath, pagePathOrUrl)
{
    var isPage = false;
    
    if (!pagePath)
    {
        throw "jsHelpers.pageIs(): pageName not specified";
        //return false;
    }
    
    pagePathOrUrl = pagePathOrUrl || document.location.pathname;
    
    // Parse the real pathname from, regardless of wither a 
    // partial path (e.g. /some/path?a=x) was supplied, or wither a full
    // URL (e.g. http://www.example.com/some/path?name=value#anchor) was supplied.
    var url = document.createElement('a');
    url.href = pagePathOrUrl;
    var cPagePath = url.pathname;
    
    // Make lower case for easier comparison.
    cPagePath = cPagePath.toLowerCase();
    
    pagePath = pagePath.toLowerCase();  // Add leading "/" for pagePath comparison.
    var cPageName = pagePath;
    if (cPageName.indexOf("/") == -1) cPageName = "/" + cPageName;    // Version Used for comparison. Leading "/" for pagePath comparison.
    
    if (cPageName === "/homepage" || cPageName === "/")
    {
        // Home page path may be "/" or "/homepage"
        if (cPagePath === "/" ||                         // e.g. "/"
            cPagePath === "/homepage" ||                 // e.g. "/homepage"
            cPagePath === "/homepage.htm" ||             // e.g. "/homepage.htm..." OR "/homepage.html..."
            cPagePath === "/homepage/"                   // e.g. "/homepage/...."
           )
           {
               isPage = true;
           }
    }
    else
    {
        // All other pages
        if (cPagePath === cPageName ||                   // e.g. "/adsl"
            cPagePath === cPageName + ".htm" ||          // e.g. "/adsl.htm..." OR "/adsl.html..."
            cPagePath === cPageName + "/"                // e.g. "/adsl/...."
           )
        {
            isPage = true;
        }
    }
    
    return isPage;
};