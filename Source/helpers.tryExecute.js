window.jsHelpers = window.jsHelpers || {};

"use strict";

window.jsHelpers.tryExecute = function(executeFunction, callback, retryIntervalMs, maxRunTimeMs)
{
    var response = null;
    
    if (typeof executeFunction != "function")
    {
        throw "jsHelpers.tryExecute(): executeFunction is not defined or not a function. type = '" + typeof executeFunction + "'.";
        
        //response = -1;  // Error.
        //return response;
    }
    if (typeof callback != "function")
    {
        console.warn("jsHelpers.tryExecute(): callback is not defined or not a function. type = '" + typeof callback + "'.");
    }
    
    if (!retryIntervalMs || retryIntervalMs <= 0) retryIntervalMs = 50;
    if (!maxRunTimeMs || maxRunTimeMs <= 0) maxRunTimeMs = 1000;

    var totalRunTime = 0;
    var timer = setInterval(function() {
        totalRunTime = totalRunTime + retryIntervalMs;
        
        // Try to execute the function
        var executeResponse = executeFunction();
        if (executeResponse == undefined || executeResponse == null)
        {
            clearInterval(timer);
            throw "jsHelpers.tryExecute()::executeFunction(): Did not return valid response. Expecting true/false.";
            
            //executeResponse = true; // Setting to true as precaution as it may have executed correctly. Don't chance re-executing the function.
            //response = -2;  // Bad response from function
        }
        else if (executeResponse == true)
        {
            response = 1;  // Successfully completed.
        }
        
        var isTimeout = (totalRunTime >= maxRunTimeMs);
        
        if (executeResponse == true || isTimeout)
        {
            // Function successfully executed, OR aximum run time reached.
            
            if (isTimeout) response = -3;   // Timeout
            clearTimeout(timer);
            if (typeof callback == "function") callback(response);
            return response;
        }
    }, retryIntervalMs);
};