window.jsHelpers = window.jsHelpers || {};

"use strict";

window.jsHelpers.getUrlParts = function(url)
{
    // Create a pseudo element
    var el = document.createElement('a');
    el.href = url;
    
    // Get the parts (and give it whatever naming I want)
    var hostname = el.hostname;
    var domainParts = hostname.split(".");
    
    var query = el.search;
    if (query.startsWith("?")) query = query.substr(1, el.search.length - 1);	// With "?" removed.
    var queryParts = query.split("&");
    
    var protocol = el.protocol;
    if (protocol.endsWith(":")) protocol = protocol.substr(0, protocol.length - 1);

    var hash = el.hash;
    if (hash.startsWith("#")) hash = hash.substr(1, hash.length - 1);
    

    var parts = {
        fullUrl: el.href,
        protocol: protocol,
        fullDomain: hostname,
        domainParts: domainParts,
        port: el.port,
        path: el.pathname,
        query: query,
        queryParts: queryParts,
        hash: hash,
    
        getSubDomain: function(rootDomain)
        {
            if (!rootDomain)
            {
                throw "jsHelpers.getUrlParts.getSubDomain(): missing rootDomain.";
            }
            console.log(this.fullDomain);
            if (!this.fullDomain)
            {
                throw "jsHelpers.getUrlParts.getSubDomain(): missing this.fullDomain.";
            }
            
            rootDomain = rootDomain.toLowerCase();
            var hostName = this.fullDomain.toLowerCase();
            var subDomain = "";
            
            if (hostname.endsWith(rootDomain))
            {
                // Remove the last occurence of the topLevelDomain, to get the sub-domain.
                var lastIndex = hostName.lastIndexOf(rootDomain);
                subDomain = hostName.substring(0, lastIndex);
                if (subDomain.length > 0 && subDomain.substr(subDomain.length - 1, 1) == ".")
                {
                    // Strip last "." if exists
                    subDomain = subDomain.substring(0, subDomain.length - 1);
                }
            }
            return subDomain;
        },
    
    
        getQueryByKey: function(key)
        {
            var value = [];
            
            for (var i = 0; i < this.queryParts.length; i++)
            {
                var parts = this.queryParts[i].split("=");
                if (parts[0].toLowerCase() == key.toLowerCase())
                {
                    if (parts.length == 1)
                    {
                        // No value specified for key.
                        value.push("");
                    }
                    else
                    {
                        value.push(decodeURIComponent(parts[1]));
                    }
                }
            }
            
            if (value.length == 0) value = null;    // Key not found in query string
            
            return value;
        }
    };
    
    return parts;
};