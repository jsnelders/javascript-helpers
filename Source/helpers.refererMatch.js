window.jsHelpers = window.jsHelpers || {};

"use strict";
 

window.jsHelpers.refererMatch = function(options)
{
    var paths = options.paths || [];
    var refererUrl = options.refererUrl || document.referrer;
    var domain = options.domain || document.location.hostname;
    
    if (refererUrl == "") return false; // No referrer
    
    var refererUrlParts = this.getUrlParts(refererUrl);
    
    if (domain.toLowerCase() != refererUrlParts.fullDomain.toLowerCase())
    {
        // Domain don't match, therefore not a match.
        return false;
    }
    
    if (paths.length == 0)
    {
        // No paths specified. Matching domain only.
        return true;
    }
    
    for (var i = 0; i < paths.length; i++)
    {
        var path = paths[i];
        if (!path.startsWith("/")) path = "/" + path;
        
        if (path.toLowerCase() === refererUrlParts.path.toLowerCase())
        {
            // Match
            return true;
        }
    }
    
    return false;
};