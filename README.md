Javascript Helpers
==================

> Author:               Jason Snelders  
> E-mail:               jason@vylesk.com  
> Source & Download:    https://bitbucket.org/jsnelders/javascript-helpers/  
> Documentation:        See /Documentation folder in this solution. (Originally also published at http://jshelpers.vylesk.com/, removed 24 Oct 2016)
 
See Index.html for the README and API documentation.

A test running exists in the test.html file in the root of the solution.

Helper source code (development and .min) can be found in the /Source folder